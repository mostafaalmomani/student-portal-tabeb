import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MarksComponent } from './marks/marks.component';
import { ExamsComponent } from './exams/exams.component';
import { ExamComponent } from './exam/exam.component';

const routes: Routes = [{
  path : 'dashboard',
  component : DashboardComponent,
  children: [
    {
    path :'marks',
    component: MarksComponent
  },
  {
    path: 'exams',
    component: ExamsComponent
  }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
