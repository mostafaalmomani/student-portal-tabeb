import { Component, OnInit } from '@angular/core';
import { ExamsService } from 'src/app/services/exams.service';

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.scss']
})
export class ExamsComponent implements OnInit {
  examID=1;
  displayedColumns: string[] = [
    'courseNumber', 'courseName', 'examDate',
    'from', 'to',
    'examNumber',
    'start'
  ];
  public dataSource = DATA_SOURCE;
  constructor(private examService:ExamsService) { }

  ngOnInit(): void {
    this.examService.getExams(this.examID).subscribe(res=>{
      console.log(res);
    })
  }

}

const DATA_SOURCE = [
  {courseNumber:69,courseName:'Maths',examDate:'4-2-2021',from:'9:00',to:'10:30',examNumber:10},
  {courseNumber:420,courseName:'Physics',examDate:'6-9-2021',from:'11:00',to:'12:30',examNumber:8}
]
