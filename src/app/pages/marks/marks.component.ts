import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MarksService } from 'src/app/services/marks.service';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.scss']
})
export class MarksComponent implements OnInit {
  courseNumber=1;
  displayedColumns: string[] = [
    'courseName', 'courseNumber', 'firstMark',
    'secondMark', 'other',
    'total', 'order', 'markAvg', 'markStatus', 'hours','grade','courseStatus'
  ];
  public dataSource = DATA_SOURCE;
  constructor(private http:HttpClient, private marksService:MarksService) { }

  ngOnInit(): void {
    this.marksService.getCourses(this.courseNumber).subscribe(res=>{
      console.log(res);
    })
  }

}

const DATA_SOURCE = [
  {courseName:'Maths',courseNumber:69,firstMark:20,secondMark:18,other:0,total:38,markAvg:25,markStatus:'Passed',hours:3,grade:'B+',courseStatus:'Good'},
  {courseName:'Physics',courseNumber:420,firstMark:16,secondMark:16,other:0,total:32,markAvg:35,markStatus:'Failed',hours:4,grade:'C+',courseStatus:'Nice'},
]