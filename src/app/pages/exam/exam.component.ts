import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {

  Questions:{num,label,answers:any,studentAnswer}[]=[];
  Answers=[];
  pages:any = [];
  numOfPages=1;
  currentPage = 0;
  returnBack = true;
  showResult = false;
  isLastPage = false;
  numOfQperPage;
  constructor(private http: HttpClient) {}
 
 ngOnInit(){
    this.getQuetions();
    this.applyRules();
    this.renderPages();
    console.log(this.Questions);
    console.log(this.pages);
  }


  // actions in page functions 
  prevQuestion(){
    console.log(this.pages);
    this.currentPage--;
    this.isLastPage = false;
  }
  nextQuestion(){
    console.log(this.pages);
    scroll(0,0);
    this.currentPage++;
    if(this.currentPage == this.numOfPages-1)
      this.isLastPage = true;
  }

  submitData(){
    console.log(this.pages);
    if(this.showResult){
      console.log('Failure');
      // navigate to exams page or logout;
    }
    else
    console.log('No reulst for now');
    // navigate to exams page or logout;
  }


  // rednring funcitions
  getQuetions(){
    Object.keys(DATA.Questions).map((q,index)=>{
      this.Questions.push({num:index+1,studentAnswer:'',label:DATA.Questions[q].label,
        answers:Object.keys(DATA.Questions[q].answers).map(a=>DATA.Questions[q].answers[a])
      });
      this.Answers.push('');
    });
  }

  renderPages(){
    // create pages that we need and
    // assign questions to those pages
    if(this.numOfPages>this.Questions.length)
      this.numOfPages = this.Questions.length;
    if(this.numOfPages <=0)
      this.numOfPages = 1;
    this.numOfQperPage = Math.ceil(this.Questions.length / this.numOfPages);
    for(let i=0;i<this.numOfPages;i++){
      this.pages.push({
        Num:i+1,
        Questions: this.Questions.slice(this.numOfQperPage * i,(this.numOfQperPage * i)+this.numOfQperPage)
      });
    };
  }

  applyRules(){
    this.numOfPages = DATA.Rules.numberOfPages;
    this.returnBack = DATA.Rules.returnBack;
    this.showResult = DATA.Rules.resultVisible;
  }


  // validation functions
  everythingIsAnswerd(){
    this.Answers = this.Questions.map(d=>d.studentAnswer);
    return this.Answers.includes('') && this.currentPage == this.numOfPages-1;
  }

  nextQuestionCheck(){
    let cPageAnswers = this.pages[this.currentPage].Questions.slice(0,(this.currentPage+1) * this.numOfQperPage);
    cPageAnswers = cPageAnswers.map(d=>d.studentAnswer);
    return cPageAnswers.includes('');
  }

}
const DATA = {
  "Rules":{
      "returnBack":true,
      "numberOfPages":3,
      "resultVisible":true
  },
  "Questions":{
      "1":{
        "label":"ما طولك؟",
        "answers":{
          "1": "فوق 150 سم" ,
          "2": "فوق 160 سم",
          "3": "فوق 170 سم",
          "4": "فوق 180 سم"
            }
      },
      "2":{
      "label":"What Kind of Flavor Do you like?",
      "answers":{
          "1":"Nuts",
          "2":"Maca",
          "3":"Ice",
          "4":"Strawberry"
          }
      },
      "3":{
      "label":"What Caffine do like?",
      "answers":{
          "1":"Tea",
          "2":"Coffe",
          "3":"Esspresses",
          "4":"Caa"
          }
      },
      "4":{
      "label":"What Framework do you use?",
      "answers":{
          "1":"React",
          "2":"Angular",
          "3":"Vue",
          "4":"Addddwqd"
          }
      },
      "5":{
        "label":"How Tall Are You? lkjqwdjqdoqd[iqwd",
        "answers":{
            "1":"Above 150cm",
            "2":"Above 160cm",
            "3":"Above 170cm",
            "4":"Above 180cm"
            }
        },
        "6":{
        "label":"What Kind of Flavor Do you like? kqwdoqofiqopiqpfuq",
        "answers":{
            "1":"Nuts",
            "2":"Maca",
            "3":"Ice",
            "4":"Strawberry"
            }
        },
        "7":{
        "label":"What Caffine do like? kqjfbjqfhoqfhoiqwefiq",
        "answers":{
            "1":"Tea",
            "2":"Coffe",
            "3":"Esspresses",
            "4":"Caa"
            }
        },
        "8":{
        "label":"What Framework do you use? qhwqjhuqgpiqg",
        "answers":{
            "1":"React",
            "2":"Angular",
            "3":"Vue",
            "4":"Addddwqd"
            }
        },
        "9":{
          "label":"How Tall Are You?",
          "answers":{
              "1":"Above 150cm",
              "2":"Above 160cm",
              "3":"Above 170cm",
              "4":"Above 180cm"
              }
          },
          "10":{
          "label":"What Kind of Flavor Do you like?",
          "answers":{
              "1":"Nuts",
              "2":"Maca",
              "3":"Ice",
              "4":"Strawberry"
              }
          },
          "11":{
          "label":"What Caffine do like?",
          "answers":{
              "1":"Tea",
              "2":"Coffe",
              "3":"Esspresses",
              "4":"Caa"
              }
          },
          "12":{
          "label":"What Framework do you use?",
          "answers":{
              "1":"React",
              "2":"Angular",
              "3":"Vue",
              "4":"Addddwqd"
              }
          },
          "13":{
            "label":"How Tall Are You?",
            "answers":{
                "1":"Above 150cm",
                "2":"Above 160cm",
                "3":"Above 170cm",
                "4":"Above 180cm"
                }
            },
            "14":{
            "label":"What Kind of Flavor Do you like?dqdqwdwqqdwqwd",
            "answers":{
                "1":"Nuts",
                "2":"Maca",
                "3":"Ice",
                "4":"Strawberry"
                }
            },
            "15":{
            "label":"What Caffine do like?",
            "answers":{
                "1":"Tea",
                "2":"Coffe",
                "3":"Esspresses",
                "4":"Caa"
                }
            },
            "16":{
            "label":"What Framework do you use? qwdqwdqwdqwdqwdqwdqwd",
            "answers":{
                "1":"React",
                "2":"Angular",
                "3":"Vue",
                "4":"Addddwqd"
                }
            },
            "17":{
              "label":"How Tall Are You?",
              "answers":{
                  "1":"Above 150cm",
                  "2":"Above 160cm",
                  "3":"Above 170cm",
                  "4":"Above 180cm"
                  }
              },
              "18":{
              "label":"What Kind of Flavor Do you like?",
              "answers":{
                  "1":"Nuts",
                  "2":"Maca",
                  "3":"Ice",
                  "4":"Strawberry"
                  }
              },
              "19":{
              "label":"What Caffine do like?",
              "answers":{
                  "1":"Tea",
                  "2":"Coffe",
                  "3":"Esspresses",
                  "4":"Caa"
                  }
              },
              "20":{
              "label":"ما طولك؟",
              "answers":{
                "1": "فوق 150 سم" ,
                "2": "فوق 160 سم",
                "3": "فوق 170 سم",
                "4": "فوق 180 سم"
                  }
              }
  }
};
