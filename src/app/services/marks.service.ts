import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MarksService {

  constructor(private http:HttpClient) { }

  getCourses(course_number){
    return this.http.get(`${environment.apiUrl}getCoursesStudents/${course_number}`);
  }

}
