import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Student } from '../core/models/';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentStudentSubject: BehaviorSubject<Student>;
  public currentStudent: Observable<Student>;
  private isAuthenticated = false;
  private token: string;
  private tokenTimer: any;
  private studentId: string;
  public studentName: string;
  private authStatusListener = new Subject<boolean>();
  constructor(private http: HttpClient) {
    this.currentStudentSubject = new BehaviorSubject<any>(localStorage.getItem('currentStudent'));
    this.currentStudent = this.currentStudentSubject.asObservable();
  }

  public get currentStudentValue(): Student {
    return this.currentStudentSubject.value;
  }

  login(studentNumber: string, password: string): any {

    return this.http.post<{ student: Student }>(`${environment.apiUrl}auth`,
      { studentNumber, password })
      .pipe(map(student => {
        if (student && student['token']) {
          const token = student['token'];
          this.token = token;
          if (token) {
            const expiresInDuration = student['expiresIn'];
            localStorage.setItem('duration', expiresInDuration);
            this.isAuthenticated = true;
            // this.studentId = student['id'];
            this.studentName = student['studentName'];
            this.authStatusListener.next(true);
            const now = new Date();
            const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
            this.saveAuthData(token, expirationDate, this.studentId, this.studentName);
            this.currentStudentSubject.next(student.student);

          }

        }

        return student;
      }));
  }
  getToken(): any {
    const token = localStorage.getItem('token');
    return token;
  }

  getIsAuth(): any {
    return this.isAuthenticated;
  }

  getStudentId(): any {
    return localStorage.getItem('currentStudent');
  }
  getAuthStatusListener(): any {
    return this.authStatusListener.asObservable();
  }


  private saveAuthData(token: string, expirationDate: Date, studentId: string, fullName: string): void {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    localStorage.setItem('currentStudent', "studentId646165496464ad65r4g65dfg");
    localStorage.setItem('fullName', fullName);
  }

  public getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    const studentId = localStorage.getItem('currentStudent');
    if (!token || !expirationDate) {
      return;
    }
    return {
      token: token,
      expirationDate: new Date(expirationDate),
      studentId: studentId
    }
  }

  public setAuthTimer(duration: number) {
    console.log(duration)
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }


  logout(): void {
    this.http.get<{ isLoggedOut: boolean }>(`${environment.apiUrl}logout`).subscribe(res => {
      localStorage.removeItem('currentStudent');
      localStorage.removeItem('token');
      localStorage.removeItem('expiration');
      this.currentStudentSubject.next(null);
      location.reload();
    });

  }
}
