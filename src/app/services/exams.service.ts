import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExamsService {

  constructor(private http:HttpClient) { }

  getExams(exam_id){
    return this.http.get(`${environment.apiUrl}getStudentsMark/${exam_id}`);
  }
}
